Navigation
==========

API list:

* [Commits](commits.md)
* [Gists](gists.md)
* [Issues](issues.md)
* [Pull Requests](pull_requests.md)
* [Repositories](repos.md)
* [Users](users.md)

Additional features:

* [Authentication & Security](security.md)
* [Request any Route](request_any_route.md)
* [Customize `php-github-api` and testing](customize.md)
